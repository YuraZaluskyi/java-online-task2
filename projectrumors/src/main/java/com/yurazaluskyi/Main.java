package com.yurazaluskyi;

import java.util.Random;

/**
 * @author Yurii Zaluskyi
 * @version 2.0 08/08/2019
 */
public class Main {
    final static int QUANTITY_GUESTS = 47;
    static AliceParty aliceParty = new AliceParty();

    public static void main(String[] args) {

        addListGuestsToAliceParty(QUANTITY_GUESTS);
        addListGuestsForSpreadRumor();
        /**
         * This is a first person who knows rumor about Alice
         * His name is Bob :-)
         * Bob's id is 0
         */
        aliceParty.getGuestFromListGuestsById(0).setKnowRumor(true);
        aliceParty.getGuestFromListGuestsById(0).setReadyToSpreadRumor(true);

        spreadRumorOnTheParty(QUANTITY_GUESTS);
        System.out.println("********************************************");
        System.out.println("Quantity guests on the Alice's party - " + QUANTITY_GUESTS);
        System.out.println("- - - - - - - - - - - -");
        System.out.println("Quantity guest which knows rumor about Alice - " + quantityGuestsWhichKnowsRumor());
        System.out.println("*********************************************");


    }

    /**
     * Method add guests to party
     *
     * @param n - quantity guests on the Alice's party
     */
    public static void addListGuestsToAliceParty(int n) {
        for (int i = 0; i < n; i++) {
            aliceParty.getListGuests().add(new Guest(i));
        }
    }

    /**
     * Method adds all guests to the guest's list except yourself
     */
    public static void addListGuestsForSpreadRumor() {
        for (Guest guest : aliceParty.getListGuests()) {
            for (int i = 0; i < QUANTITY_GUESTS; i++) {
                if (i == guest.getId()) {
                    continue;
                }
                guest.getListGuestsForSpreadRumor().add(new Guest(i));
            }
        }
    }

    public static int getRandomNumberFromZeroToN(int n) {
        return new Random().nextInt(n);
    }

    public static boolean isEmptyListGuestForSpreadRumor(Guest guest) {
        if (guest.getListGuestsForSpreadRumor().size() == 0) {
            return true;
        }
        return false;
    }

    /**
     * The method is looking for a guest among guests on list guests for spread rumor
     *
     * @param guest
     * @return - guest from a list guests for spread rumor
     */
    public static Guest getRandomGuestFromListGuestsForSpreadRumor(Guest guest) {
        int size = aliceParty.getGuestFromListGuestsById(guest.getId()).getListGuestsForSpreadRumor().size();
        int randomNumber = getRandomNumberFromZeroToN(size);
        return aliceParty.getGuestFromListGuestsById(guest.getId()).getListGuestsForSpreadRumor().get(randomNumber);
    }

    /**
     * Method check anybody ready to spread a rumor
     * To spread the rumor, there should be someone who knows it and is ready to tell the other guest
     *
     * @return - true or false
     */
    public static boolean isAnybodyReadyToSpreadRumor() {
        for (Guest guest : aliceParty.getListGuests()) {
            if (guest.isReadyToSpreadRumor() == true) {
                return true;
            }
        }
        return false;
    }

    public static int quantityGuestsWhichKnowsRumor() {
        int amount = 0;
        for (Guest guest : aliceParty.getListGuests()) {
            if (guest.isKnowRumor()) {
                amount++;
            }
        }
        return amount;
    }


    /**
     * guestWhoSpreadRumor - guest who spreads rumor.
     * The first is Bob.
     * guestWhoSpreadRumor is looking for among its list random guest(guestForRumor) in oder to spread rumor.
     * After that the guestWhoSpreadRumor delete guestForRumor from its list and the
     * guestForRumor delete guestWhoSpreadRumor from its list.
     * Then we check guestForRumor's fields isKnowRumor and isReadyToSpreadRumor
     * If guestForRumor does not know rumor, he will be next guestWhoSpreadRumor
     *
     * @param quantityGuestOnTheParty
     */

    public static void spreadRumorOnTheParty(int quantityGuestOnTheParty) {
        Guest guestWhoSpreadRumor = aliceParty.getGuestFromListGuestsById(0);
        while (guestWhoSpreadRumor.isReadyToSpreadRumor() && !isEmptyListGuestForSpreadRumor(guestWhoSpreadRumor)) {
            Guest guestFromListForSpreadRumor = getRandomGuestFromListGuestsForSpreadRumor(guestWhoSpreadRumor);
            Guest guestForRumor = aliceParty.getGuestFromListGuestsById(guestFromListForSpreadRumor.getId());

            guestForRumor.deleteGuestFromListGuestsForSpreadRumor(guestWhoSpreadRumor);
            guestWhoSpreadRumor.deleteGuestFromListGuestsForSpreadRumor(guestForRumor);

            if (!guestForRumor.isKnowRumor()) {
                guestForRumor.setKnowRumor(true);
                guestForRumor.setReadyToSpreadRumor(true);
            } else {
                guestForRumor.setReadyToSpreadRumor(false);
            }
            guestWhoSpreadRumor = guestForRumor;

        }
    }
}
