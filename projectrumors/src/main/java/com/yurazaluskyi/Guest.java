package com.yurazaluskyi;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yurii Zaluskyi
 * @version 2.0 08/08/2019
 *
 */

public class Guest {

    private int id;
    private boolean isKnowRumor = false;
    private boolean isReadyToSpreadRumor = false;

    /**
     * Everyone guest has a list of people who can tell the rumor
     */
    private List<Guest> listGuestsForSpreadRumor = new ArrayList<Guest>();

    public Guest() {
    }

    public Guest(int id) {
        this.id = id;
    }

    public Guest(int id, List<Guest> listGuestsForSpreadRumor) {
        this.id = id;
        this.listGuestsForSpreadRumor = listGuestsForSpreadRumor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isKnowRumor() {
        return isKnowRumor;
    }

    public void setKnowRumor(boolean knowRumor) {
        isKnowRumor = knowRumor;
    }

    public boolean isReadyToSpreadRumor() {
        return isReadyToSpreadRumor;
    }

    public void setReadyToSpreadRumor(boolean readyToSpreadRumor) {
        isReadyToSpreadRumor = readyToSpreadRumor;
    }

    public List<Guest> getListGuestsForSpreadRumor() {
        return listGuestsForSpreadRumor;
    }

    public void setListGuestsForSpreadRumor(List<Guest> listGuestsForSpreadRumor) {
        this.listGuestsForSpreadRumor = listGuestsForSpreadRumor;
    }

    public void deleteGuestFromListGuestsForSpreadRumor(Guest guest) {
        listGuestsForSpreadRumor.remove(guest);
    }

    public boolean isGuestOnTheListGuestsForSpreadRumor(Guest guest) {
        return listGuestsForSpreadRumor.contains(guest);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Guest guest = (Guest) o;

        return id == guest.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "Guest{" +
                "id=" + id +
                ", isKnowRumor=" + isKnowRumor +
                ", isReadyToSpreadRumor=" + isReadyToSpreadRumor +
                '}' + "\n";
    }
}
