package com.yurazaluskyi;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yurii Zaluskyi
 * @version 2.0 08/08/2019
 */
public class AliceParty {

    private List<Guest> listGuests = new ArrayList<Guest>();

    public AliceParty() {
    }


    public List<Guest> getListGuests() {
        return listGuests;
    }

    public void setListGuests(List<Guest> listGuests) {
        this.listGuests = listGuests;
    }

    public Guest getGuestFromListGuestsById(int id) {
        for (Guest guest : listGuests) {
            if(guest.getId() == id){
                return guest;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "AliceParty{" +
                "listGuests=" + "\n" + listGuests +
                '}';
    }
}
